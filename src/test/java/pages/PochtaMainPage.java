package pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;


import java.io.ByteArrayInputStream;

import static com.codeborne.selenide.Selenide.*;


public class PochtaMainPage{
    private final SelenideElement enterLogin = $("#username");
    //private final SelenideElement clickAuthorization = $("#username");
    private final SelenideElement setPassword = $("#userpassword");
    private final SelenideElement clickFinnishAuthorization = $(" #loginForm > button");




    @Step("Открытие главной страницы")
    public PochtaMainPage open() {
        Selenide.open("");
        return this;
    }


    @Step("Ввод логина")
    public PochtaMainPage setLogin(String text) {
        enterLogin.setValue(text);
        return this;
    }

    @Step("Нажатие на кнопку авторизации")
    public PochtaMainPage clickAuthorization() {
        Selenide.$(" a[href=\"/api/auth/login\"]").click();
        return this;
    }


    @Step("Ввод пароля")
    public PochtaMainPage setPassword(String password) {
        setPassword.setValue(password);
        return this;

    }

    @Step("Завершение авторизации")
    public PersonalAccount clickFinnishAuthorization() {
        clickFinnishAuthorization.click();
    return new PersonalAccount();



    }
}