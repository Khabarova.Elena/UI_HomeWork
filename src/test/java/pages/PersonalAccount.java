package pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.junit.jupiter.api.Assertions;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;



public class PersonalAccount extends BasePage{


    @Step("Наведение на кнопку аккаунта")
    public PersonalAccount actionToAccount() {
        Selenide.actions()
                .moveToElement($("div[title='79917133197']"))
                .build().perform();
        getScreenshot();
        return this;
    }

    @Step("Выбор пункта Отправления")
    public PersonalAccount chooseDelivery(){
        $("div[id='tippy-1'] a:nth-child(1)").click();
        return this;
    }

    @Step("Ожидание текста -Сохраняйте отправления, чтобы узнавать по электронной почте, где они находятся")
    public PersonalAccount waitForText() {
        String actual = $("p.Paragraph-sc-10hckd4-0.fhoCWJ").getText();
        String expected = "Сохраняйте отправления, чтобы узнавать по электронной почте, где они находятся";
        Assertions.assertEquals(expected, actual);
        getScreenshot();
        return this;
    }

}

