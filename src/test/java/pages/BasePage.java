package pages;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;

import java.util.Base64;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
public class BasePage {

    @Attachment(value = "screen", type = "image/png")
    public byte[] getScreenshot() {
        String screenshotAsBase64 = Selenide.screenshot(OutputType.BASE64);
        return Base64.getDecoder().decode(screenshotAsBase64);

    }

    public static Logger log = Logger.getLogger(String.valueOf(BasePage.class));

    public static void main(String[] args) {
        PropertyConfigurator.configure("(C:\\~\\log4j.properties");


    }
}

