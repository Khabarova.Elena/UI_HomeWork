import com.codeborne.selenide.Configuration;
import io.qameta.allure.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.BasePage;
import pages.PersonalAccount;
import pages.PochtaMainPage;


@DisplayName("Авторизация на сайте и поиск текста")
public class MainPochtaTest extends BasePage {


    @BeforeAll
    public static void setUp(){
        Configuration.timeout = 15;
        Configuration.baseUrl = "https://www.pochta.ru/";
        Configuration.holdBrowserOpen = false;
    }



    @Test
    @Story("Авторизация на сайте и поиск текста")
    @Epic("Авторизация на сайте почты и поиск текста")
    @Description("Проверка наличия текста в личном кабинете")
    @Severity(SeverityLevel.BLOCKER)
    @DisplayName("Проверка отображения текста в личном кабинете")
    public void HomeWorkTest(){
        new PochtaMainPage()
                .open()
                .clickAuthorization()
                .setLogin("tojoxi2718@mahazai.com")
                .setPassword("1QAZ2wsx")
                .clickFinnishAuthorization();
        log.info("Авторизация выполнена");
        new PersonalAccount()
                        .actionToAccount()
                        .chooseDelivery()
                        .waitForText();
        log.info("Текст соответствует ожидаемому");



    }


    }




